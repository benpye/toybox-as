
// STATIC:		"ALPHATEST"				"0..1"
// DYNAMIC:		"SHADOW_MODE"			"0..2"

#include "common_ps_fxc.h"
#include "common_deferred_fxc.h"


#if ALPHATEST
sampler sAlbedo							: register( s0 );

const float g_flAlphaRef				: register( c0 );
#endif

struct PS_INPUT
{
	float2 vTexCoord				: TEXCOORD0;

#if SHADOWMAPPING_USE_COLOR
#	if SHADOW_MODE == DEFERRED_SHADOW_MODE_PROJECTED
	float2 flProjZW					: TEXCOORD1;
#	else
	float flDepth					: TEXCOORD1;
#	endif
#endif

#if SHADOW_MODE == DEFERRED_SHADOW_MODE_DPSM
	float flBack					: TEXCOORD2;
#endif
};

struct PS_OUTPUT
{
	float4 vColor_0				:	COLOR0;
};

PS_OUTPUT main( const PS_INPUT In )
{
#if SHADOW_MODE == DEFERRED_SHADOW_MODE_DPSM
	clip( In.flBack );
#endif

	PS_OUTPUT Out;

#if ALPHATEST
	float alpha = tex2D( sAlbedo, In.vTexCoord ).a;

	clip( alpha - g_flAlphaRef );
#endif

#if SHADOWMAPPING_USE_COLOR
#	if SHADOW_MODE == DEFERRED_SHADOW_MODE_PROJECTED
	float depth = In.flProjZW.x / In.flProjZW.y;
	Out.vColor_0 = float4( depth.xxx, 1 );
#	else
	Out.vColor_0 = float4( In.flDepth.xxx, 1 );
#	endif
#else
	Out.vColor_0 = float4( 1, 1, 1, 1 );
#endif
	return Out;
}