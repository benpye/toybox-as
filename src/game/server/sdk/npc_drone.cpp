//==== Copyright � 2012, Sandern Corporation, All rights reserved. =========
//
//
//=============================================================================

#include "cbase.h"
#include "ai_baseactor.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

ConVar	sk_drone_swipe_damage( "sk_drone_swipe_damage", "5" );

#define	DRONE_MELEE1_RANGE		100.0f

ConVar	sk_drone_health( "sk_drone_health","20");

Activity ACT_MELEE_ATTACK1_HIT;
Activity ACT_MELEE_ATTACK2_HIT;
int AE_ALIEN_MELEE_HIT;

Activity ACT_ALIEN_FLINCH_SMALL;
Activity ACT_ALIEN_FLINCH_MEDIUM;
Activity ACT_ALIEN_FLINCH_BIG;
Activity ACT_ALIEN_FLINCH_GESTURE;
Activity ACT_DEATH_FIRE;
Activity ACT_DEATH_ELEC;
Activity ACT_DIE_FANCY;

Activity ACT_BURROW_IDLE;
Activity ACT_BURROW_OUT;

int AE_DRONE_MELEE_HIT1;

class CNPC_Drone : public CAI_BaseActor
{
	DECLARE_CLASS( CNPC_Drone, CAI_BaseActor );

public:
	CNPC_Drone();
	~CNPC_Drone();

	//---------------------------------

	void			Precache();
	void			Spawn();
	void			Activate();

	Class_T			Classify();
	
	void			SetupGlobalModelData();
	Activity		NPC_TranslateActivity( Activity baseAct );
	virtual	bool	OverrideMoveFacing( const AILocalMoveGoal_t &move, float flInterval );

	virtual float	GetIdealSpeed( ) const;
	virtual float	GetSequenceGroundSpeed( CStudioHdr *pStudioHdr, int iSequence );

	virtual void		DeathSound( const CTakeDamageInfo &info );
	virtual void		AlertSound( void );
	virtual void		IdleSound( void );
	virtual void		PainSound( const CTakeDamageInfo &info );
	//virtual void		FearSound( void );

	virtual void	MeleeAttack( float distance, float damage, QAngle &viewPunch, Vector &shove );
	virtual void	HandleAnimEvent( animevent_t *pEvent );

	void			StartTask( const Task_t *pTask );
	void			RunTask( const Task_t *pTask );

	void			RunAttackTask( int task );

public:
	static int gm_nMoveXPoseParam;
	static int gm_nMoveYPoseParam;
	static int gm_nLeanYawPoseParam;
	static int gm_nLeanPitchPoseParam;

private:
	DEFINE_CUSTOM_AI;

	DECLARE_DATADESC();

	int m_iAttackLayer;
};

LINK_ENTITY_TO_CLASS( npc_drone, CNPC_Drone );


BEGIN_DATADESC( CNPC_Drone )
END_DATADESC()

int CNPC_Drone::gm_nMoveXPoseParam = -1;
int CNPC_Drone::gm_nMoveYPoseParam = -1;
int CNPC_Drone::gm_nLeanYawPoseParam = -1;
int CNPC_Drone::gm_nLeanPitchPoseParam = -1;

CNPC_Drone::CNPC_Drone()
{

}

CNPC_Drone::~CNPC_Drone()
{

}

void CNPC_Drone::Precache()
{
	PrecacheModel( "models/aliens/drone/drone.mdl" );

	BaseClass::Precache();
}

void CNPC_Drone::Spawn()
{
	Precache();

	SetModel( "models/aliens/drone/drone.mdl" );
	BaseClass::Spawn();

	SetHullType( HULL_MEDIUMBIG );
	SetHullSizeNormal();
	SetDefaultEyeOffset();
	
	SetNavType( NAV_GROUND );
	m_NPCState = NPC_STATE_NONE;
	
	m_iHealth = m_iMaxHealth = sk_drone_health.GetInt();

	m_flFieldOfView		= 0.2;

	SetSolid( SOLID_BBOX );
	AddSolidFlags( FSOLID_NOT_STANDABLE );
	SetMoveType( MOVETYPE_STEP );

	SetupGlobalModelData();
	
	CapabilitiesAdd( bits_CAP_MOVE_GROUND );
	CapabilitiesAdd( bits_CAP_INNATE_MELEE_ATTACK1 );

	NPCInit();
}

void CNPC_Drone::Activate()
{
	BaseClass::Activate();

	SetupGlobalModelData();
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
Class_T CNPC_Drone::Classify()
{
	return CLASS_ZOMBIE;
}

void CNPC_Drone::SetupGlobalModelData()
{
	return;
}

float CNPC_Drone::GetIdealSpeed( ) const
{
	return 300;
}

float CNPC_Drone::GetSequenceGroundSpeed( CStudioHdr *pStudioHdr, int iSequence )
{
	// Ensure navigator will move
	// TODO: Could limit it to move sequences only.
	float speed = BaseClass::GetSequenceGroundSpeed( pStudioHdr, iSequence );
	if( speed <= 0 /*&& GetSequenceActivity( iSequence) == ACT_TERROR_RUN_INTENSE*/ ) speed = 1.0f;
	return speed;
}

Activity CNPC_Drone::NPC_TranslateActivity( Activity baseAct )
{

	return baseAct;
}

bool CNPC_Drone::OverrideMoveFacing( const AILocalMoveGoal_t &move, float flInterval )
{
	// required movement direction
	float flMoveYaw = UTIL_VecToYaw( move.dir );

	// FIXME: move this up to navigator so that path goals can ignore these overrides.
	Vector dir;
	float flInfluence = GetFacingDirection( dir );
	dir = move.facing * (1 - flInfluence) + dir * flInfluence;
	VectorNormalize( dir );

	// ideal facing direction
	float idealYaw = UTIL_AngleMod( UTIL_VecToYaw( dir ) );
		
	// FIXME: facing has important max velocity issues
	GetMotor()->SetIdealYawAndUpdate( idealYaw );	

	// find movement direction to compensate for not being turned far enough
	float flDiff = UTIL_AngleDiff( flMoveYaw, GetLocalAngles().y );

	// Setup the 9-way blend parameters based on our speed and direction.
	Vector2D vCurMovePose( 0, 0 );

	vCurMovePose.x = cos( DEG2RAD( flDiff ) ) * 1.0f; //flPlaybackRate;
	vCurMovePose.y = -sin( DEG2RAD( flDiff ) ) * 1.0f; //flPlaybackRate;

	SetPoseParameter( gm_nMoveXPoseParam, vCurMovePose.x );
	SetPoseParameter( gm_nMoveYPoseParam, vCurMovePose.y );

	// ==== Update Lean pose parameters
	if ( gm_nLeanYawPoseParam >= 0 )
	{
		float targetLean = GetPoseParameter( gm_nMoveYPoseParam ) * 30.0f;
		float curLean = GetPoseParameter( gm_nLeanYawPoseParam );
		if( curLean < targetLean )
			curLean += MIN(fabs(targetLean-curLean), GetAnimTimeInterval()*15.0f);
		else
			curLean -= MIN(fabs(targetLean-curLean), GetAnimTimeInterval()*15.0f);
		SetPoseParameter( gm_nLeanYawPoseParam, curLean );
	}

	if( gm_nLeanPitchPoseParam >= 0 )
	{
		float targetLean = GetPoseParameter( gm_nMoveXPoseParam ) * -30.0f;
		float curLean = GetPoseParameter( gm_nLeanPitchPoseParam );
		if( curLean < targetLean )
			curLean += MIN(fabs(targetLean-curLean), GetAnimTimeInterval()*15.0f);
		else
			curLean -= MIN(fabs(targetLean-curLean), GetAnimTimeInterval()*15.0f);
		SetPoseParameter( gm_nLeanPitchPoseParam, curLean );
	}

	return true;
}

void CNPC_Drone::MeleeAttack( float distance, float damage, QAngle &viewPunch, Vector &shove )
{
	Vector vecForceDir;

	// Always hurt bullseyes for now
	if ( ( GetEnemy() != NULL ) && ( GetEnemy()->Classify() == CLASS_BULLSEYE ) )
	{
		vecForceDir = (GetEnemy()->GetAbsOrigin() - GetAbsOrigin());
		CTakeDamageInfo info( this, this, damage, DMG_SLASH );
		CalculateMeleeDamageForce( &info, vecForceDir, GetEnemy()->GetAbsOrigin() );
		GetEnemy()->TakeDamage( info );
		return;
	}

	CBaseEntity *pHurt = CheckTraceHullAttack( distance, -Vector(16,16,32), Vector(16,16,32), damage, DMG_SLASH, 5.0f );

	if ( pHurt )
	{
		vecForceDir = ( pHurt->WorldSpaceCenter() - WorldSpaceCenter() );

		//FIXME: Until the interaction is setup, kill combine soldiers in one hit -- jdw
		if ( FClassnameIs( pHurt, "npc_combine_s" ) )
		{
			CTakeDamageInfo	dmgInfo( this, this, pHurt->m_iHealth+25, DMG_SLASH );
			CalculateMeleeDamageForce( &dmgInfo, vecForceDir, pHurt->GetAbsOrigin() );
			pHurt->TakeDamage( dmgInfo );
			return;
		}

		CBasePlayer *pPlayer = ToBasePlayer( pHurt );

		if ( pPlayer != NULL )
		{
			//Kick the player angles
			if ( !(pPlayer->GetFlags() & FL_GODMODE ) && pPlayer->GetMoveType() != MOVETYPE_NOCLIP )
			{
				pPlayer->ViewPunch( viewPunch );

				Vector	dir = pHurt->GetAbsOrigin() - GetAbsOrigin();
				VectorNormalize(dir);

				QAngle angles;
				VectorAngles( dir, angles );
				Vector forward, right;
				AngleVectors( angles, &forward, &right, NULL );

				//Push the target back
				pHurt->ApplyAbsVelocityImpulse( - right * shove[1] - forward * shove[0] );
			}
		}

		// Play a random attack hit sound
		EmitSound( "Zombie.Punch" );
	}
	else
	{
		EmitSound( "Zombie.AttackMiss" );
	}
}

void CNPC_Drone::HandleAnimEvent( animevent_t *pEvent )
{
	int nEvent = pEvent->Event();

	/*if ( nEvent ==  AE_FOOTSTEP_RIGHT )
	{
		EmitSound( "Zombie.FootstepLeft", pEvent->eventtime );
		return;
	}

	if ( nEvent ==  AE_FOOTSTEP_LEFT )
	{
		EmitSound( "Zombie.FootstepRight", pEvent->eventtime );
		return;
	}*/


	if ( pEvent->Event() == AE_DRONE_MELEE_HIT1 )
	{
		MeleeAttack( DRONE_MELEE1_RANGE, sk_drone_swipe_damage.GetFloat(), QAngle( 20.0f, 0.0f, -12.0f ), Vector( -250.0f, 1.0f, 1.0f ) );
		return;

	}
	BaseClass::HandleAnimEvent( pEvent );
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CNPC_Drone::PainSound( const CTakeDamageInfo &info )
{
	// We're constantly taking damage when we are on fire. Don't make all those noises!
	if ( IsOnFire() )
	{
		return;
	}

	EmitSound( "Zombie.Pain" );
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
void CNPC_Drone::DeathSound( const CTakeDamageInfo &info ) 
{
	EmitSound( "Zombie.Die" );
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CNPC_Drone::AlertSound( void )
{
	EmitSound( "Zombie.Alert" );
}

//-----------------------------------------------------------------------------
// Purpose: Play a random idle sound.
//-----------------------------------------------------------------------------
void CNPC_Drone::IdleSound( void )
{
	EmitSound( "Zombie.Wander" );
}

void CNPC_Drone::StartTask( const Task_t *pTask )
{
	BaseClass::StartTask( pTask );
}

void CNPC_Drone::RunTask( const Task_t *pTask )
{
	BaseClass::RunTask( pTask );
}

void CNPC_Drone::RunAttackTask( int task )
{
	AutoMovement( );

	Vector vecEnemyLKP = GetEnemyLKP();

	// If our enemy was killed, but I'm not done animating, the last known position comes
	// back as the origin and makes the me face the world origin if my attack schedule
	// doesn't break when my enemy dies. (sjb)
	if( vecEnemyLKP != vec3_origin )
	{
		if ( ( task == TASK_RANGE_ATTACK1 || task == TASK_RELOAD ) && 
			 ( CapabilitiesGet() & bits_CAP_AIM_GUN ) && 
			 FInAimCone( vecEnemyLKP ) )
		{
			// Arms will aim, so leave body yaw as is
			GetMotor()->SetIdealYawAndUpdate( GetMotor()->GetIdealYaw(), AI_KEEP_YAW_SPEED );
		}
		else
		{
			GetMotor()->SetIdealYawToTargetAndUpdate( vecEnemyLKP, AI_KEEP_YAW_SPEED );
		}
	}

	CAnimationLayer *pPlayer = GetAnimOverlay( m_iAttackLayer );
	if ( pPlayer->m_bSequenceFinished )
	{
		if ( task == TASK_RELOAD && GetShotRegulator() )
		{
			GetShotRegulator()->Reset( false );
		}

		TaskComplete();
	}
}

//-------------------------------------------------------------------------------------------------
//
// Schedules
//
//-------------------------------------------------------------------------------------------------
AI_BEGIN_CUSTOM_NPC( npc_drone, CNPC_Drone )
	DECLARE_ACTIVITY( ACT_MELEE_ATTACK1_HIT )
	DECLARE_ACTIVITY( ACT_MELEE_ATTACK2_HIT )
	DECLARE_ACTIVITY( AE_ALIEN_MELEE_HIT )

	DECLARE_ACTIVITY( ACT_ALIEN_FLINCH_SMALL )
	DECLARE_ACTIVITY( ACT_ALIEN_FLINCH_MEDIUM )
	DECLARE_ACTIVITY( ACT_ALIEN_FLINCH_BIG )
	DECLARE_ACTIVITY( ACT_ALIEN_FLINCH_GESTURE )
	DECLARE_ACTIVITY( ACT_DEATH_FIRE )
	DECLARE_ACTIVITY( ACT_DEATH_ELEC )
	DECLARE_ACTIVITY( ACT_DIE_FANCY )

	DECLARE_ANIMEVENT( AE_DRONE_MELEE_HIT1 )

AI_END_CUSTOM_NPC()
