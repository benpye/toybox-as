
// STATIC:		"MODEL"					"0..1"
// STATIC:		"TANGENTSPACE"			"0..1"
// STATIC:		"MORPHING_VTEX"			"0..1"

// DYNAMIC:		"COMPRESSED_VERTS"		"0..1"
// DYNAMIC:		"SKINNING"				"0..1"
// DYNAMIC:		"MORPHING"				"0..1"


#include "common_vs_fxc.h"
#include "common_deferred_fxc.h"

static const bool g_bSkinning		= SKINNING ? true : false;

const float3 g_vecOrigin					: register( SHADER_SPECIFIC_CONST_0 );
const float3 g_vecForward					: register( SHADER_SPECIFIC_CONST_1 );

const float g_flDepthScale					: register( SHADER_SPECIFIC_CONST_2 );

#if MODEL

const float3 cMorphTargetTextureDim			: register( SHADER_SPECIFIC_CONST_10 );
const float4 cMorphSubrect					: register( SHADER_SPECIFIC_CONST_11 );
sampler2D morphSampler						: register( D3DVERTEXTEXTURESAMPLER0, s0 );

#endif

struct VS_INPUT
{
#if MODEL
	float4 vPos						: POSITION;
	float4 vNormal					: NORMAL;
#else
	float3 vPos						: POSITION;
	float3 vNormal					: NORMAL;
#endif
	float2 vTexCoord_0				: TEXCOORD0;

#if MODEL

	float4 vBoneWeights				: BLENDWEIGHT;
	float4 vBoneIndices				: BLENDINDICES;
	float4 vUserData				: TANGENT;

	float4 vTexCoord2				: TEXCOORD2;
	float4 vPosFlex					: POSITION1;
	float4 vNormalFlex				: NORMAL1;
	float vVertexID					: POSITION2;

#else

	float3 vTangentS				: TANGENT;
	float3 vTangentT				: BINORMAL;

#endif
};

struct VS_OUTPUT
{
	float4 vProjPos					: POSITION;
	float2 vTexCoord				: TEXCOORD0;

	float3 worldNormal				: TEXCOORD1;

#if TANGENTSPACE
	float3 worldTangentS			: TEXCOORD2;
	float3 worldTangentT			: TEXCOORD3;
#endif

	float depth						: TEXCOORD4;
};

VS_OUTPUT main( const VS_INPUT In )
{
	VS_OUTPUT Out;

	float3 worldPos;
	float3 worldNormal;

	// unpack stuff (model) &&
	// morph (model) &&
	// skin (model) ||
	// xform to world (not model)

#if MODEL
	float4 vPos = In.vPos;
	float3 vNormal = 0;

#if TANGENTSPACE
	float4 vTangentMixed = 0;
	float3 worldTangentS = 0;
	float3 worldTangentT = 0;

	DecompressVertex_NormalTangent( In.vNormal, In.vUserData, vNormal, vTangentMixed );

#if MORPHING
#if !MORPHING_VTEX
	ApplyMorph( In.vPosFlex, In.vNormalFlex, vPos.xyz, vNormal, vTangentMixed.xyz );
#else
	ApplyMorph( morphSampler, cMorphTargetTextureDim, cMorphSubrect,
		In.vVertexID, In.vTexCoord2, vPos.xyz, vNormal, vTangentMixed.xyz );
#endif
#endif

	SkinPositionNormalAndTangentSpace( g_bSkinning, vPos, vNormal, vTangentMixed,
		In.vBoneWeights, In.vBoneIndices,
		worldPos, worldNormal, worldTangentS, worldTangentT );

	worldTangentS = normalize( worldTangentS );
	worldTangentT = normalize( worldTangentT );

#else // TANGENTSPACE
	DecompressVertex_Normal( In.vNormal, vNormal );

#if MORPHING
#if !MORPHING_VTEX
	ApplyMorph( In.vPosFlex, In.vNormalFlex, vPos.xyz, vNormal );
#else
	ApplyMorph( morphSampler, cMorphTargetTextureDim, cMorphSubrect,
		In.vVertexID, In.vTexCoord2, vPos.xyz, vNormal );
#endif
#endif

	SkinPositionAndNormal( g_bSkinning, vPos, vNormal,
		In.vBoneWeights, In.vBoneIndices,
		worldPos, worldNormal );

#endif // NOT TANGENTSPACE

	worldNormal = normalize( worldNormal );

#else // MODEL

	worldPos = mul( float4( In.vPos, 1 ), cModel[0] );
	worldNormal = mul( In.vNormal, ( float3x3 )cModel[0] );

#if TANGENTSPACE

	float3 worldTangentS = mul( In.vTangentS, ( float3x3 )cModel[0] );
	float3 worldTangentT = mul( In.vTangentT, ( float3x3 )cModel[0] );

#endif // NOT TANGENTSPACE

#endif // NOT MODEL

	Out.vProjPos = mul( float4( worldPos, 1 ), cViewProj );
	Out.depth = WriteDepth( worldPos, g_vecOrigin, g_vecForward, g_flDepthScale );

	Out.vTexCoord = In.vTexCoord_0;

	Out.worldNormal = worldNormal;

#if TANGENTSPACE
	Out.worldTangentS = worldTangentS;
	Out.worldTangentT = worldTangentT;
#endif

	return Out;
}