"Resource/UI/CampaignFlyout.res"
{
	"PnlBackground"
	{
		"ControlName"			"Panel"
		"fieldName"				"PnlBackground"
		"xpos"					"0"
		"ypos"					"0"
		"zpos"					"-1"
		"wide"					"180" [$ENGLISH]
		"wide"					"270" [!$ENGLISH]
		"tall"					"45"
		"visible"				"1"
		"enabled"				"1"
		"paintbackground"		"1"
		"paintborder"			"1"
	}

	"BtnPlayOnGroupServer"
	{
		"ControlName"			"BaseModHybridButton"
		"fieldName"				"BtnPlayOnGroupServer"
		"xpos"					"0"
		"ypos"					"0"
		"wide"					"150"
		"tall"					"20"
		"autoResize"			"1"
		"pinCorner"				"0"
		"visible"				"1"
		"enabled"				"1"
		"tabPosition"			"0"
		"wrap"					"1"
		"navUp"					"BtnCreateGame"
		"navDown"				"BtnCreateGame"
		"labelText"				"#L4D360UI_MainMenu_DedicatedServerBrowser"
		"tooltiptext"			"#L4D360UI_MainMenu_DedicatedServerBrowser"
		"disabled_tooltiptext"	"#L4D360UI_MainMenu_DedicatedServerBrowser"
		"style"					"FlyoutMenuButton"
		"command"				"OpenServerBrowser"
	}

	"BtnCreateGame"
	{
		"ControlName"			"BaseModHybridButton"
		"fieldName"				"BtnCreateGame"
		"xpos"					"0"
		"ypos"					"20"
		"wide"					"150"
		"tall"					"20"
		"autoResize"			"1"
		"pinCorner"				"0"
		"visible"				"1"
		"enabled"				"1"
		"tabPosition"			"0"
		"wrap"					"1"
		"navUp"					"BtnPlayOnGroupServer"
		"navDown"				"BtnPlayOnGroupServer"
		"labelText"				"#L4D360UI_MainMenu_CoOp_New"
		"tooltiptext"			"#L4D360UI_MainMenu_CoOp_New_Tip"
		"disabled_tooltiptext"	"#L4D360UI_MainMenu_CoOp_New_Tip"
		"style"					"FlyoutMenuButton"
		"command"				"CreateGame"
	}
}